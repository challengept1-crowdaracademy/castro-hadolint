FROM ubuntu:20.04
USER root
RUN apt update && apt upgrade -y && apt dist-upgrade -y
RUN  apt-get update \
  && apt-get install -y wget \
  && rm -rf /var/lib/apt/lists/*
RUN wget -O /bin/hadolint https://github.com/hadolint/hadolint/releases/download/v2.4.0/hadolint-Linux-x86_64
RUN chmod +x /bin/hadolint
